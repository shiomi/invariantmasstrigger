#include "TriggerLateStudy.h"
#include "/home/shiomi/RootUtils/RootUtils/TLegend_addfunc.h"
#include "/home/shiomi/RootUtils/RootUtils/TCanvas_opt.h"
#include "TStyle.h"
#include "TF1.h"
#include "THStack.h"
#include "TLegend.h"

using namespace std;

void TriggerLateStudy::DrawHist(TString pdf)
{
    TCanvas_opt *c1 = new TCanvas_opt();
    gStyle->SetOptStat(0);
    c1->SetTopMargin(0.20);
    c1->Print(pdf + "[", "pdf");

    h_massB->Draw();
    h_massB->SetLineColor(kOrange+8);
    h_massB->SetFillColor(kOrange+8);
    h_massB->SetFillStyle(3335);
    h_massU->Draw("same");
    h_massU->SetLineColor(kAzure+10);
    h_massU->SetFillColor(kAzure+10);
    h_massU->SetFillStyle(3335);
    c1->Print(pdf,"pdf");

    h_dR->Draw();
    h_dR->SetLineColor(kAzure-4);
    h_dR->SetFillColor(kAzure-4);
    h_dR->SetFillStyle(3015);
    c1->Print(pdf,"pdf");

    h_NEvents->Draw("hist text");
    h_NEvents->SetLineColor(kAzure);
    h_NEvents->GetXaxis()->SetBinLabel(1,"All");
    h_NEvents->GetXaxis()->SetBinLabel(2,"2Muon");
    h_NEvents->GetXaxis()->SetBinLabel(3,"3sta");
    h_NEvents->GetXaxis()->SetBinLabel(4,"HotRoI");
    h_NEvents->GetXaxis()->SetBinLabel(5,"OvlpRmvl");
    h_NEvents->GetXaxis()->SetBinLabel(6,"Charge");
    h_NEvents->GetXaxis()->SetBinLabel(7,"B");
    h_NEvents->GetXaxis()->SetBinLabel(8,"U");
    c1->Print(pdf,"pdf");

    h_MdR->Draw("colz");
    c1->Print(pdf,"pdf");

    h_pT->Draw("colz");
    c1->Print(pdf,"pdf");

    h_etaphi->Draw("colz");
    c1->Print(pdf,"pdf");

    T_eta->Draw("hist");
    T_eta->SetFillColor(kMagenta+2);
    T_eta->SetLineWidth(1);
    S_eta->Draw("hist same");
    S_eta->SetFillColor(kOrange+10);
    S_eta->SetLineWidth(1);
    H_eta->Draw("hist same");
    H_eta->SetFillColor(kOrange-2);
    H_eta->SetLineWidth(1);
    O_eta->Draw("hist same");
    O_eta->SetFillColor(kAzure+1);
    O_eta->SetLineWidth(1);
    C_eta->Draw("hist same");
    C_eta->SetFillColor(kCyan+1);
    C_eta->SetLineWidth(1);
    B_eta->Draw("hist same");
    B_eta->SetFillColor(kGreen+2);
    B_eta->SetLineWidth(1);
    c1->Print(pdf,"pdf");

    T_eta->Draw("hist");
    T_eta->SetFillColor(kMagenta+2);
    T_eta->SetLineWidth(1);
    S_eta->Draw("hist same");
    S_eta->SetFillColor(kOrange+10);
    S_eta->SetLineWidth(1);
    H_eta->Draw("hist same");
    H_eta->SetFillColor(kOrange-2);
    H_eta->SetLineWidth(1);
    O_eta->Draw("hist same");
    O_eta->SetFillColor(kAzure+1);
    O_eta->SetLineWidth(1);
    C_eta->Draw("hist same");
    C_eta->SetFillColor(kCyan+1);
    C_eta->SetLineWidth(1);
    U_eta->Draw("hist same");
    U_eta->SetFillColor(kGreen+2);
    U_eta->SetLineWidth(1);
    c1->Print(pdf,"pdf");

    c1 -> Print( pdf + "]", "pdf" );
    delete c1;
}
