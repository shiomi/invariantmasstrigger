#include "TriggerLateStudy.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <typeinfo>
#include "TH1F.h"
#include "TF1.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TROOT.h"
#include "TFile.h"
#include "THStack.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TString.h"
#include "TProfile.h"
#include "TColor.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TPaveText.h"
#include "TText.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include "TApplication.h"
#include "TVector3.h"
#include "TVector2.h"
#include "TH1.h"
#include "map"

using namespace std;

void TriggerLateStudy::Mass()
{
    int candidate = RoI_pt.size();
    vector<float> BEta;BEta.clear();
    vector<float> UEta;UEta.clear();
    vector<float> SEta;SEta.clear();

    for(int i=0;i!=candidate;i++){
        for(int j=i+1;j!=candidate;j++){
            bool sign = false;
            /*
            if(RoI_source.at(i)!=0 && RoI_source.at(j)!=0){
                if(RoI_charge.at(i)==1 && RoI_charge.at(j)==1){ sign=true; }
                if(RoI_charge.at(i)==0 && RoI_charge.at(j)==0){ sign=true; }
            }
            if(sign) continue;
            */
            SEta.push_back(RoI_eta.at(i));
            SEta.push_back(RoI_eta.at(j));
            /*
            bool ptsign = false;
            if(RoI_source.at(i)==0 && RoI_source.at(j)==0){
                if(RoI_pt.at(i)==4 || RoI_pt.at(j)==4){ptsign=true;}
            }
            else if(RoI_source.at(i)!=0 && RoI_source.at(j)!=0){
                if(RoI_pt.at(i)==3 || RoI_pt.at(j)==3){ptsign=true;}
            }
            else if(RoI_source.at(i)==0 && RoI_source.at(j)!=0){
                if(RoI_pt.at(i)==4 || RoI_pt.at(j)==3){ptsign=true;}
            }
            else if(RoI_source.at(i)!=0 && RoI_source.at(j)==0){
                if(RoI_pt.at(i)==3 || RoI_pt.at(j)==4){ptsign=true;}
            }
            if(!ptsign) continue;
            */
            float eta_cal = cosh(RoI_eta.at(i) - RoI_eta.at(j));
            float phi_cal = cos(RoI_phi.at(i) - RoI_phi.at(j));
            float mass = 2*RoI_pt.at(i)*RoI_pt.at(j)*(eta_cal - phi_cal);

            float deta =abs(RoI_eta.at(i) - RoI_eta.at(j));
            float dphi=TVector2::Phi_mpi_pi(RoI_phi.at(i) - RoI_phi.at(j));
            float dR=sqrt(deta*deta + dphi*dphi);
            h_pT->Fill(RoI_pt.at(i),RoI_pt.at(j));
            h_dR->Fill(dR);
            h_MdR->Fill(sqrt(mass),dR);
            
            if(dR>=0.3 && dR<=2.0){
                h_massB->Fill(sqrt(mass));
                if(sqrt(mass)>=2 && sqrt(mass)<=8){
                    BEta.push_back(RoI_eta.at(i));
                    BEta.push_back(RoI_eta.at(j));
                }
            }
            if(dR>=0.5 && dR<=3.0){
                h_massU->Fill(sqrt(mass));
                if(sqrt(mass)>=8 && sqrt(mass)<=17){
                    UEta.push_back(RoI_eta.at(i));
                    UEta.push_back(RoI_eta.at(j));
                }
            }
        }
    }
    if(SEta.size()!=0){
    h_NEvents->Fill(5);
    for(int i=0;i!=(int)SEta.size();i++){
        C_eta->Fill(SEta.at(i),(float)1/(float)SEta.size());
    }
    }
    if(BEta.size()!=0){
    h_NEvents->Fill(6);
    for(int i=0;i!=(int)BEta.size();i++){
        B_eta->Fill(BEta.at(i),(float)1/(float)BEta.size());
    }
    }
    if(UEta.size()!=0){
    h_NEvents->Fill(7);
    for(int i=0;i!=(int)UEta.size();i++){
        U_eta->Fill(UEta.at(i),(float)1/(float)UEta.size());
    }
    }
}
