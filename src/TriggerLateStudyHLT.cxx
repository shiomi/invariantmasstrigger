#include "../include/TriggerLateStudy.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <typeinfo>

using namespace std;

bool TriggerLateStudy::HLT()
{
    bool HLTFlag = false;
    for(int i=0; i!=HLT_info_n; i++){
        //if((*HLT_info_chain)[i] == "L1_MU20"){
        //if((*HLT_info_chain)[i]=="L1_BPH-2M9-2MU6_BPH-2DR15-2MU6"){
        if((*HLT_info_chain)[i]=="L1_BPH-8M15-2MU6_BPH-0DR22-2MU6"){
            if((*HLT_info_isPassed)[i] == 1){
                HLTFlag = true;
            }
        }
    }
    return HLTFlag;
}
