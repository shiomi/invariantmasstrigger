#include "TriggerLateStudy.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <typeinfo>
#include "TH1F.h"
#include "TF1.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TROOT.h"
#include "TFile.h"
#include "THStack.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TString.h"
#include "TProfile.h"
#include "TColor.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TPaveText.h"
#include "TText.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include "TApplication.h"
#include "TVector3.h"
#include "TVector2.h"
#include "TH1.h"

using namespace std;

void TriggerLateStudy::InitHist()
{
    h_massB = new TH1D("h_massB","B;M^{L1}_{#mu#mu} [GeV];Events",60,0,20);
    h_massU = new TH1D("h_massU","U;M^{L1}_{#mu#mu} [GeV];Events",60,0,20);
    h_dR = new TH1D("h_dR",";#DeltaR;Events",50,0,4.0);
    h_pT = new TH2D("h_pT",";p_{T};p_{T}",21,0,21,21,0,21);
    h_MdR = new TH2D("h_MdR",";M;dR",36,0,18,35,0,3.5);
    h_etaphi = new TH2D("h_etaphi",";#eta;#phi",100,-2.5,2.5,96,-TMath::Pi(),TMath::Pi());
    h_NEvents = new TH1D("h_NEvents",";;Events",8,0,8);
    A_eta = new TH1D("A_eta",";#eta^{RoI};Events",75,-2.7,2.7);
    T_eta = new TH1D("T_eta",";#eta^{RoI};Events",75,-2.7,2.7);
    S_eta = new TH1D("S_eta",";#eta^{RoI};Events",75,-2.7,2.7);
    H_eta = new TH1D("H_eta",";#eta^{RoI};Events",75,-2.7,2.7);
    O_eta = new TH1D("O_eta",";#eta^{RoI};Events",75,-2.7,2.7);
    B_eta = new TH1D("B_eta",";#eta^{RoI};Events",75,-2.7,2.7);
    U_eta = new TH1D("U_eta",";#eta^{RoI};Events",75,-2.7,2.7);
    C_eta = new TH1D("C_eta",";#eta^{RoI};Events",75,-2.7,2.7);
}

void TriggerLateStudy::End()
{
    if(h_massU!=0){delete h_massU;}
    if(h_massB!=0){delete h_massB;}
    if(h_dR!=0){delete h_dR;}
    if(h_pT!=0){delete h_pT;}
    if(h_MdR!=0){delete h_MdR;}
    if(h_etaphi!=0){delete h_etaphi;}
    if(h_NEvents!=0){delete h_NEvents;}
    if(A_eta!=0){delete A_eta;}
    if(T_eta!=0){delete T_eta;}
    if(C_eta!=0){delete C_eta;}
    if(H_eta!=0){delete H_eta;}
    if(O_eta!=0){delete O_eta;}
    if(U_eta!=0){delete U_eta;}
    if(B_eta!=0){delete B_eta;}
    if(S_eta!=0){delete S_eta;}
}
