#include "TriggerLateStudy.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <typeinfo>
#include "TH1F.h"
#include "TF1.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TROOT.h"
#include "TFile.h"
#include "THStack.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TTree.h"
#include "TChain.h"
#include "TMath.h"
#include "TString.h"
#include "TProfile.h"
#include "TColor.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TPaveText.h"
#include "TText.h"
#include "TLatex.h"
#include "TGraphErrors.h"
#include "TApplication.h"
#include "TVector3.h"
#include "TVector2.h"
#include "TH1.h"

using namespace std;

bool TriggerLateStudy::HotRoI(int tgc)
{
    int phisector = (*TGC_Run3_PhiSector)[tgc];
    int roi = (*TGC_Run3_RoI)[tgc];
    bool Flag = false;

    if((*TGC_Run3_IsEndcap)[tgc]){
            if(phisector%6==1){
            if(roi==44 || roi==48 || roi==52){Flag = true;}
            //if(roi==40 || roi==44 || roi==45 || roi==48 || roi==49 || roi==52){Flag = true;}
            }
            if(phisector%6==0){
            if(roi==52){Flag = true;}
            //if(roi==44 || roi==45 || roi==48 || roi==49 || roi==52){Flag = true;}
            }
            if(phisector%6==3){
            if(roi==75 || roi==79 || roi==83 || roi==87 || roi==91 || roi==94 || roi==95 || roi==99){Flag = true;}
            }
            if(phisector%6==4){
            if(roi==79 || roi==83 || roi==87 || roi==91 || roi==95 || roi==99){Flag = true;}
            }
    }
    return Flag;
}
